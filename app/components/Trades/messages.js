/*
 * Trades Messages
 *
 * This contains all the text for the Trades component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Trades.header',
    defaultMessage: 'This is the Trades component !',
  },
});
