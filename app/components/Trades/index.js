/**
*
* Trades
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { last } from 'lodash';

import RaisedButton from 'material-ui/RaisedButton';

import TradeTable from './TradeTable';

import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const Wrapper = styled.div`
  margin-right: 50px;
`

const HeaderWrapper = styled.div`
  border: solid thin black;
  border-radius: 0.4em;
  margin-top: 0.3em;
  padding: 0.5em;
  width: 29em;

  span {
    margin: 1em;
  }
`

class Trades extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      openTrades: [],
      openedTrades: [],
      closedTrades: [],
      profit: 0.00
    }
  }

  calc () {
    let openTrades = [];
    let openedTrades = [];
    let closedTrades = [];
    let profit = 0.00;
    let tradeValue = 100;

    this.props.trades.map((trade, index) => {
      if (trade.type === 'BUY') {
        const mappedTrade = this.mapTrade(trade);
        openTrades.push(mappedTrade);
        openedTrades.push(mappedTrade);
      } else {
        const openingTrade = openedTrades.filter(x => x.currency === trade.currency).splice(-1)[0];
        openTrades = openTrades.filter(x => x.currency !== trade.currency);

        const tradeProfit = this.calcTradeProfit(openingTrade, trade);

        closedTrades.push(this.mapTrade(trade, tradeProfit.toFixed(2)));

        profit += tradeProfit;
      }
    });

    this.setState({
      openTrades,
      openedTrades,
      closedTrades,
      profit
    })
  }

  componentWillMount () {
    this.calc();
  }

  componentWillReceiveProps (nextProps) {
    this.calc();
  }

  calcTradeProfit = (openingTrade, closingTrade) => {
    const quantity = closingTrade.amount;
    const profit = (closingTrade.price - openingTrade.price ) * quantity * 5000;
    return profit;
  }

  mapTrade = (trade, profit = 0) => {
    if(profit === 0) {
      const currency = trade.currency.replace('BTC_', '')
      const currencyData = this.props.currencyData.get(currency);
      if( currencyData ) {
        const currentPrice = currencyData.get('currentPrice');
        profit = (currentPrice - trade.price) * trade.amount * 5000;
      }
    }

    return {
      currency: trade.currency,
      date: trade.date,
      type: trade.type,
      price: trade.price,
      amount: trade.amount,
      profit: profit
    }
  }

  render() {

    return (
      <Wrapper>
        <HeaderWrapper>
          <RaisedButton onClick={ this.props.onRequestTradeHistory } label='Load Trade History' />
          <span>Profit: {this.state.profit.toFixed(2)}</span>
          <span>Open Trades: {this.props.openTrades.size}</span>
        </HeaderWrapper>

        <h3>Closed Trades</h3>
        <TradeTable trades={this.state.closedTrades} height={150} />

        <h3>Open Trades</h3>
        <TradeTable trades={this.state.openTrades} />

        <h3>Opened Trades</h3>
        <TradeTable trades={this.props.openedTrades} height={150} />
      </Wrapper>
    );
  }
}

Trades.propTypes = {
  trades: PropTypes.object,
  openTrades: PropTypes.object,
  openedTrades: PropTypes.object,
  closedTrades: PropTypes.object,
  currencyData: PropTypes.object,
  onRequestTradeHistory: PropTypes.func.isRequired
};

export default Trades;
