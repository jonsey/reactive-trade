/*
 * MarketTriggers Messages
 *
 * This contains all the text for the MarketTriggers component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.MarketTriggers.header',
    defaultMessage: 'Currencies with a buy trigger',
  },
});
