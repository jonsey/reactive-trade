import React from 'react';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Toggle from 'material-ui/Toggle';
import ActionHome from 'material-ui/svg-icons/action/home';

const TradeTable = (props) => {
  return (
    <Table
        height={'150px'}
        fixedHeader={true}
        fixedFooter={true}
        selectable={true}
      >
        <TableHeader
          displaySelectAll={true}
          adjustForCheckbox={true}
          enableSelectAll={true}
        >
          <TableRow>
            <TableHeaderColumn colSpan="5" tooltip="Triggers" style={{textAlign: 'center'}}>
              Triggers
            </TableHeaderColumn>
          </TableRow>
          <TableRow>
            <TableHeaderColumn tooltip="The Currency">Currency</TableHeaderColumn>
            <TableHeaderColumn tooltip="The ADX Trigger">Date</TableHeaderColumn>
            <TableHeaderColumn tooltip="The ADX Trigger">Type</TableHeaderColumn>
            <TableHeaderColumn tooltip="The ADX Trigger">Price</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody
          displayRowCheckbox={true}
          deselectOnClickaway={true}
          showRowHover={true}
          stripedRows={true}
        >
          {props.triggers.map((data, index) => (
            <TableRow key={`${index}`}>
              <TableRowColumn>{data.currency}</TableRowColumn>
              <TableRowColumn>{data.date}</TableRowColumn>
              <TableRowColumn>{data.type}</TableRowColumn>
              <TableRowColumn>{data.price}</TableRowColumn>

            </TableRow>
            ))}
        </TableBody>
      </Table>
  )
}

export default TradeTable;
