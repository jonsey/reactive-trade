/**
*
* MarketTriggers
*
*/

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Toggle from 'material-ui/Toggle';
import ActionHome from 'material-ui/svg-icons/action/home';

// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import TriggerTable from './TriggerTable';


class MarketTriggers extends React.PureComponent  {

  constructor(props) {
    super(props);
    this.state = {
      buyTriggers: [],
      sellTriggers: []
    }
  }

  componentWillMount () {
    this.buildTriggers();
  }

  componentWillReceiveProps (nextProps) {
    this.buildTriggers();
  }

  buildTriggers () {
    let buyTriggers = [];
    let sellTriggers = [];

    this.props.triggers.slice(-10).map((trigger, index) => {
      const mappedTrigger = this.mapTrigger(trigger);
      if (trigger.type === 'BUY') {
        buyTriggers.push(mappedTrigger);
      } else {
        sellTriggers.push(mappedTrigger);
      }
    });

    this.setState({
      buyTriggers,
      sellTriggers
    })
  }

  mapTrigger = (trigger) => {
    return {
      currency: trigger.currency,
      date: new Date(trigger.date * 1000).toString(),
      type: trigger.type,
      price: trigger.price
    }
  }

  render () {

    return (
      <div style={{ marginRight: '50px'}}>
        <div>

        </div>
        <h3>Buy Triggers</h3>
        <TriggerTable triggers={this.state.buyTriggers} />
        <h3>Sell Triggers</h3>
        <TriggerTable triggers={this.state.sellTriggers} />
      </div>
    )
  }
}

MarketTriggers.propTypes = {
  triggers: PropTypes.object,
  showTriggers: PropTypes.bool
};

export default MarketTriggers;
