/**
 *
 * Asynchronously loads the component for MarketTriggers
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
