/*
 * Adxgraph Messages
 *
 * This contains all the text for the Adxgraph component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Adxgraph.header',
    defaultMessage: 'This is the Adxgraph component !',
  },
});
