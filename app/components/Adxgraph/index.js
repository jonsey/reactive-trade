/**
*
* Adxgraph
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const Wrapper = styled.div `
  margin-left: 0%;
`;

/* eslint-disable no-undef */
const build = (data, ref) => {
  if(ref && data) {
    const margin = { top: 20, right: 20, bottom: 30, left: 50 };
    const width = 850 - margin.left - margin.right;
    const height = 250 - margin.top - margin.bottom;

    const x = techan.scale.financetime()
              .range([0, width]);

    const y = d3.scaleLinear()
              .range([height, 0]);

    const adx = techan.plot.adx()
              .xScale(x)
              .yScale(y);

    const xAxis = d3.axisBottom(x);

    const yAxis = d3.axisLeft(y)
              .tickFormat(d3.format(',.3s'));

    d3.selectAll(`#${ref} > svg`).remove();
    const svg = d3.select(`#${ref}`).append('svg')
              .attr('width', width + margin.left + margin.right)
              .attr('height', height + margin.top + margin.bottom)
              .append('g')
              .attr('transform', `translate(${margin.left},${margin.top})`);


    const accessor = adx.accessor();

    const mappedData = data.map((d) =>
         ({
           date: new Date(d.date * 1000),
           volume: +d.quoteVolume,
           open: +d.open,
           high: +d.high,
           low: +d.low,
           close: +d.close,
         })).sort((a, b) => d3.ascending(accessor.d(a), accessor.d(b)));

    svg.append('g')
              .attr('class', 'adx');

    svg.append('g')
              .attr('class', 'x axis')
              .attr('transform', `translate(0,${height})`);

    svg.append('g')
              .attr('class', 'y axis')
              .append('text')
              .attr('transform', 'rotate(-90)')
              .attr('y', 6)
              .attr('dy', '.71em')
              .style('text-anchor', 'end')
              .text('Average Directional Index');

    draw(mappedData);


    function draw(drawData) {
      const adxData = techan.indicator.adx()(drawData);
      x.domain(adxData.map(adx.accessor().d));
      y.domain(techan.scale.plot.adx(adxData).domain());

      svg.selectAll('g.adx').datum(adxData).call(adx);
      svg.selectAll('g.x.axis').call(xAxis);
      svg.selectAll('g.y.axis').call(yAxis);
    }
  }

};

/* eslint-enable no-undef */

function Adxgraph(props) {
  if (props.data && props.data.size > 0) {
    const ref = `adx_${props.currency}`;
    return (
      <Wrapper id={ref} ref={(container) => { build(props.data.get('rawData'), ref) }}>
        <FormattedMessage {...messages.header} />
        <div>{`BTC_${props.currency}`}</div>
      </Wrapper>
    );
  } else {
    return false;
  }
}

Adxgraph.propTypes = {
  data: PropTypes.array,
  currency: PropTypes.string,
};

export default Adxgraph;
