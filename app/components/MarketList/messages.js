/*
 * MarketList Messages
 *
 * This contains all the text for the MarketList component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.MarketList.header',
    defaultMessage: 'This is the MarketList component !',
  },
});
