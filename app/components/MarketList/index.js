/**
*
* MarketTriggers
*
*/

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Toggle from 'material-ui/Toggle';
import ActionHome from 'material-ui/svg-icons/action/home';

// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';


class MarketList extends React.PureComponent  {


  render () {

    return (
      <Table
          height={'100%'}
          fixedHeader={true}
          fixedFooter={true}
          selectable={true}
        >
          <TableHeader
            displaySelectAll={true}
            adjustForCheckbox={true}
            enableSelectAll={true}
          >

            <TableRow>
              <TableHeaderColumn tooltip="The Currency">Currency</TableHeaderColumn>
              <TableHeaderColumn tooltip="The ADX Trigger">ADX</TableHeaderColumn>
              <TableHeaderColumn tooltip="The ADX Trigger">RSI</TableHeaderColumn>
              <TableHeaderColumn tooltip="The ADX Trigger">MACD</TableHeaderColumn>
              <TableHeaderColumn tooltip="Go to currency detail"></TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={true}
            deselectOnClickaway={true}
            showRowHover={true}
            stripedRows={true}
          >

            {this.props.triggers.map((data, key) => (
              <TableRow key={key}>
                <TableRowColumn>{key}</TableRowColumn>
                <TableRowColumn>{data.get('adxData').slice(-1)[0].adx}</TableRowColumn>
                <TableRowColumn>{data.get('rsiData').slice(-1)[0].rsi}</TableRowColumn>
                <TableRowColumn>{data.get('macdData').slice(-1)[0].signal}</TableRowColumn>
                <TableRowColumn>
                  <IconButton tooltip="SVG Icon">
                    <ActionHome />
                  </IconButton>
                </TableRowColumn>
              </TableRow>
              ))}
          </TableBody>
        </Table>
    )
  }
}

MarketList.propTypes = {
  triggers: PropTypes.object,
  showTriggers: PropTypes.bool
};

export default MarketList;
