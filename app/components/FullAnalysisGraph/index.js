/**
*
* FullAnalysisGraph
*
*/

import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import { build } from './builder';

const Wrapper = styled.div `
  margin-left: 0%;
`;

class FullAnalysisGraph extends React.PureComponent {

  render () {
    const { data, currency, trades, period } = this.props;

    if (data && data.size > 0) {
      const reference = `${currency}_${period}`;
      return (
        <Wrapper id={ reference } ref={ (container) => { build(data.get('rawData').toJS(), period, trades, currency, reference) } }>
        </Wrapper>
      );
    } else {
      return false;
    }
  }
}

FullAnalysisGraph.propTypes = {
  data: PropTypes.object,
  trades: PropTypes.object,
  currency: PropTypes.string,
  period: PropTypes.number
};

export default FullAnalysisGraph;
