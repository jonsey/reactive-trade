/*
 * TradingToolbar Messages
 *
 * This contains all the text for the TradingToolbar component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.TradingToolbar.header',
    defaultMessage: 'This is the TradingToolbar component !',
  },
});
