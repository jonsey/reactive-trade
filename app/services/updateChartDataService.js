import { last } from 'lodash';

export const updateChartDataService = (currencyData, ticker) => {
  let newCurrencyData;

  if (currencyData.size > 0 && currencyData.has(ticker.id)) {
    const currentDate = Math.round(Date.now() / 1000);

    currencyData.map((periodData, currency) => {
      const period = periodData.keySeq().toArray()[0];

      newCurrencyData = currencyData.updateIn([currency, period, 'rawData'], rawData => {
        const lastCandle = last(rawData);
        const candleDate = lastCandle.date;

        if (candleDate < (currentDate - period)) {
          // get chart data again
        }

        if(candleDate >= (currentDate - period) && candleDate < currentDate) {
          lastCandle.close = ticker.last;
          lastCandle.high = lastCandle.high < ticker.last ? ticker.last : lastCandle.high;
          lastCandle.low = lastCandle.low > ticker.last ? ticker.last : lastCandle.low;
        }

        rawData[rawData.length -1] = lastCandle;
        return rawData;
      })
    })
  }

  return newCurrencyData;
}
