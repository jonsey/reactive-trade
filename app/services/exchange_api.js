import request from 'utils/request';

export const fetchChartData = (currency, start, period) => {
  const requestURL = `https://poloniex.com/public?command=returnChartData&currencyPair=${currency}&start=${start}&end=9999999999&period=${period}`;

  const result = request(requestURL).then(data => {
    const adxData = techan.indicator.adx()(data);
    const rsiData = techan.indicator.rsi()(data);
    const macdData = techan.indicator.macd()(data);
    const bb = techan.indicator.bollinger()(data);
    const atrTrailing = techan.indicator.atrtrailingstop()(data);

    return { data, adxData, rsiData, macdData, bb, atrTrailing, currency };
  });

  return result;
}
