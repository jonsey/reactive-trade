/**
 *
 * InstrumentPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectInstrumentPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import { checkTheMarket } from 'containers/MarketAnalysisProvider/actions';
import { makeSelectCurrentInstrument } from './selectors';
import { makeSelectCurrencyData,
  makeSelectTrades,
  makeSelectTriggers } from 'containers/MarketAnalysisProvider/selectors';

import FullAnalysisGraph from 'components/FullAnalysisGraph';
import Adxgraph from 'components/Adxgraph';

export class InstrumentPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor (props) {
    super(props)

    this.state = {
      instrument: props.instrument,
      trades: []
    }
  }
  instrumentChanged = (e) => {
    const instrument = e.target.value;
    this.setState({
      instrument: instrument
    })
  }

  render() {
    const { data, trades } = this.props;
    const { instrument } = this.state;
    const currencyTrades = trades.filter(trade => trade.currency === `BTC_${instrument.toUpperCase()}`);

    return (
      <div>
        <FormattedMessage {...messages.header} />
        <TextField onChange={ this.instrumentChanged } />
        <RaisedButton text='Get Data' onClick={ (e) => this.props.onGetInstrumentData(this.state.instrument) } />
        <FullAnalysisGraph data={ data.get(instrument) } trades={currencyTrades} currency={ instrument } />
        <Adxgraph data={ data.get(instrument) } currency={ instrument } />
      </div>
    );
  }
}

InstrumentPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  onGetInstrumentData: PropTypes.func.isRequired,
  instrument: PropTypes.string,
  data: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  instrument: makeSelectCurrentInstrument(),
  data: makeSelectCurrencyData(),
  trades: makeSelectTrades()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onGetInstrumentData: (instrument) => dispatch(checkTheMarket(instrument))
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'instrumentPage', reducer });
const withSaga = injectSaga({ key: 'instrumentPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(InstrumentPage);
