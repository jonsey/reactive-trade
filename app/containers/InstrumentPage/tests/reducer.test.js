
import { fromJS } from 'immutable';
import instrumentPageReducer from '../reducer';

describe('instrumentPageReducer', () => {
  it('returns the initial state', () => {
    expect(instrumentPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
