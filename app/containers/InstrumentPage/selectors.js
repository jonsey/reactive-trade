import { createSelector } from 'reselect';

/**
 * Direct selector to the instrumentPage state domain
 */
const selectInstrumentPageDomain = (state) => state.get('instrumentPage');
const selectMarketAnalysisProviderDomain = (state) => state.get('marketAnalysisProvider');

/**
 * Other specific selectors
 */
export const makeSelectCurrentInstrument = (instrument) => createSelector(
  selectInstrumentPageDomain,
  (substate) => substate.get('currentInstrument')
)

/**
 * Default selector used by InstrumentPage
 */

const makeSelectInstrumentPage = () => createSelector(
  selectInstrumentPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectInstrumentPage;
export {
  selectInstrumentPageDomain,
};
