/*
 * InstrumentPage Messages
 *
 * This contains all the text for the InstrumentPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.InstrumentPage.header',
    defaultMessage: 'This is InstrumentPage container !',
  },
});
