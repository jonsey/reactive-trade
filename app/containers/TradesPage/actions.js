/*
 *
 * TradesPage actions
 *
 */

import {
  DEFAULT_ACTION,
  REQUEST_TRADE_HISTORY
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function requestTradeHistory() {
  return {
    type: REQUEST_TRADE_HISTORY
  }
}
