/*
 *
 * TradesPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  REQUEST_TRADE_HISTORY
} from './constants';

const initialState = fromJS({});

function tradesPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default tradesPageReducer;
