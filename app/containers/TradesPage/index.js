/**
 *
 * TradesPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectTradesPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import Trades from 'components/Trades';

import { requestTradeHistory } from './actions';
import { makeSelectTrades,
  makeSelectOpenTrades,
  makeSelectOpenedTrades,
  makeSelectClosedTrades,
  makeSelectCurrencyData } from 'containers/MarketAnalysisProvider/selectors';

export class TradesPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { trades,
      openTrades,
      openedTrades,
      closedTrades,
      currencyData,
      onRequestTradeHistory } = this.props;

    return (
      <div>
        <h2>Trade History</h2>
        <Trades
          trades={ trades }
          openTrades={ openTrades }
          openedTrades={ openedTrades }
          closedTrades={ closedTrades }
          currencyData={ currencyData }
          onRequestTradeHistory={ onRequestTradeHistory }/>
      </div>
    );
  }
}

TradesPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  onRequestTradeHistory: PropTypes.func.isRequired,
  trades: PropTypes.object,
  openTrades: PropTypes.object,
  openedTrades: PropTypes.object,
  closedTrades: PropTypes.object,
  currencyData: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  trades: makeSelectTrades(),
  openTrades: makeSelectOpenTrades(),
  openedTrades: makeSelectOpenedTrades(),
  closedTrades: makeSelectClosedTrades(),
  currencyData: makeSelectCurrencyData()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onRequestTradeHistory: () => dispatch(requestTradeHistory())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'tradesPage', reducer });
const withSaga = injectSaga({ key: 'tradesPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(TradesPage);
