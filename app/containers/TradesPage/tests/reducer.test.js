
import { fromJS } from 'immutable';
import tradesPageReducer from '../reducer';

describe('tradesPageReducer', () => {
  it('returns the initial state', () => {
    expect(tradesPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
