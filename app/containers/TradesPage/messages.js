/*
 * TradesPage Messages
 *
 * This contains all the text for the TradesPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.TradesPage.header',
    defaultMessage: 'This is TradesPage container !',
  },
});
