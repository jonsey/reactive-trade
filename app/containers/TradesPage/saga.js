import { takeEvery, call, put, select } from 'redux-saga/effects';
import { makeSelectTradingChannel } from '../MarketAnalysisProvider/selectors'
import { REQUEST_TRADE_HISTORY } from './constants';

function* requestTradeHistory () {
  const tradingChannel = yield select(makeSelectTradingChannel());
  tradingChannel.push("trade:all_history", {});
}

// Individual exports for testing
export default function* defaultSaga() {
  yield takeEvery(REQUEST_TRADE_HISTORY, requestTradeHistory);
}
