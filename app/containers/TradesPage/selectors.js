import { createSelector } from 'reselect';

/**
 * Direct selector to the tradesPage state domain
 */
const selectTradesPageDomain = (state) => state.get('tradesPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by TradesPage
 */

const makeSelectTradesPage = () => createSelector(
  selectTradesPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectTradesPage;
export {
  selectTradesPageDomain,
};
