/*
 *
 * TradesPage constants
 *
 */

export const DEFAULT_ACTION = 'app/TradesPage/DEFAULT_ACTION';
export const REQUEST_TRADE_HISTORY = 'app/TradesPage/REQUEST_TRADE_HISTORY';
