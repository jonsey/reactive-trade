/*
 *
 * MarketAnalysisProvider constants
 *
 */

export const DEFAULT_ACTION = 'app/MarketAnalysisProvider/DEFAULT_ACTION';
export const SETUP_PHOENIX_SOCKET = 'app/MarketAnalysisProvider/SETUP_PHOENIX_SOCKET';
export const TRADING_CHANNEL_JOINED = 'app/MarketAnalysisProvider/TRADING_CHANNEL_JOINED';
export const TICKER_CHANNEL_JOINED = 'app/MarketAnalysisProvider/TICKER_CHANNEL_JOINED';
export const CURRENCY_CHANGE = 'app/MarketAnalysisProvider/CURRENCY_CHANGE';
export const CHECK_THE_MARKET = 'app/MarketAnalysisProvider/CHECK_THE_MARKET';
export const MARKET_DATA_LOADED = 'app/MarketAnalysisProvider/MARKET_DATA_LOADED';
export const BUY_TRIGGERED = 'app/MarketAnalysisProvider/BUY_TRIGGERED';
export const SELL_TRIGGERED = 'app/MarketAnalysisProvider/SELL_TRIGGERED';
export const TRADE_OPENED = 'app/MarketAnalysisProvider/TRADE_OPENED';
export const TRADE_CLOSED = 'app/MarketAnalysisProvider/TRADE_CLOSED';
export const TICKER_RECIEVED = 'app/MarketAnalysisProvider/TICKER_RECIEVED';
export const CHART_DATA_LOADING_ERROR = 'app/MarketAnalysisProvider/CHART_DATA_LOADING_ERROR';
export const UPDATE_RAW_DATA = 'app/MarketAnalysisProvider/UPDATE_RAW_DATA';
export const UPDATE_CURRENCY_PERIOD_RAW_DATA = 'app/MarketAnalysisProvider/UPDATE_CURRENCY_PERIOD_RAW_DATA';
export const UPDATE_CURRENCY_PERIOD_CHART_DATA = 'app/MarketAnalysisProvider/UPDATE_CURRENCY_PERIOD_CHART_DATA';

// export const CURRENCIES = [
//   'BTC_VTC',
//   'BTC_VRC',
//   'BTC_FCT',
//   'BTC_DCR',
//   'BTC_VIA',
//   'BTC_NEOS',
//   'BTC_GNO'
// ]

export const CURRENCIES = [
  'BTC_ARDR',
  'BTC_BURST',
  'BTC_DASH',
  'BTC_DCR',
  'BTC_DGB',
  'BTC_EMC2',
  'BTC_ETC',
  'BTC_FCT',
  'BTC_FLDC',
  'BTC_FLO',
  'BTC_GAME',
  'BTC_GNO',
  'BTC_GNT',
  'BTC_GRC',
  'BTC_HUC',
  'BTC_LBC',
  'BTC_LSK',
  'BTC_LTC',
  'BTC_NAUT',
  'BTC_NEOS',
  'BTC_NXT',
  'BTC_OMNI',
  'BTC_PINK',
  'BTC_PPC',
  'BTC_SC',
  'BTC_STEEM',
  'BTC_STRAT',
  'BTC_SYS',
  'BTC_VRC',
  'BTC_VTC',
  'BTC_XCP',
  'BTC_XMR',,
  'BTC_XRP',
  'BTC_ZEC'
]
