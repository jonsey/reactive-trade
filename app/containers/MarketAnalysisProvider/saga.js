import { takeEvery, takeLatest } from 'redux-saga/effects';

import {
  SETUP_PHOENIX_SOCKET,
  TICKER_CHANNEL_JOINED,
  TRADING_CHANNEL_JOINED,
  TICKER_RECIEVED,
  CHECK_THE_MARKET,
  MARKET_DATA_LOADED,
  BUY_TRIGGERED,
  SELL_TRIGGERED } from './constants';

import buyTriggeredSaga from './sagas/buyTriggeredSaga';
import sellTriggeredSaga from './sagas/sellTriggeredSaga';
import setupSocketSaga from './sagas/setupSocketsSaga';
import tickerChannelJoinedSaga from './sagas/tickerChannelJoinedSaga';
import tradingChannelJoinedSaga from './sagas/tradingChannelJoinedSaga';
import updateChartDataSaga from './sagas/updateChartDataSaga';
import updateRawDataSaga from './sagas/updateRawDataSaga';
import getChartDataSaga from './sagas/getChartDataSaga';
import checkTriggersSaga from './sagas/checkTriggersSaga';
import checkSellTriggerSaga from './sagas/checkSellTriggerSaga';


export default function* chartData() {
  yield takeEvery(CHECK_THE_MARKET, getChartDataSaga);
  yield takeEvery(MARKET_DATA_LOADED, checkTriggersSaga);
  // yield takeEvery(TICKER_RECIEVED, checkTriggersSaga);

  yield takeLatest(SETUP_PHOENIX_SOCKET, setupSocketSaga);
  yield takeLatest(TICKER_CHANNEL_JOINED, tickerChannelJoinedSaga);
  yield takeLatest(TRADING_CHANNEL_JOINED, tradingChannelJoinedSaga);

  // yield takeEvery(TICKER_RECIEVED, updateChartDataSaga);
  yield takeEvery(TICKER_RECIEVED, updateRawDataSaga);
  yield takeEvery(TICKER_RECIEVED, checkSellTriggerSaga);

  yield takeEvery(BUY_TRIGGERED, buyTriggeredSaga);
  yield takeEvery(SELL_TRIGGERED, sellTriggeredSaga);
}
