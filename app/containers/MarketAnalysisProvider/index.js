/**
 *
 * MarketAnalysisProvider
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import reducer from './reducer';
import saga from './saga';
import { checkTheMarket,
  setupPhoenixSocket} from './actions';

import { CURRENCIES }from './constants';

export class MarketAnalysisProvider extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      intervalId: null
    }
  }

  componentWillMount() {
    this.props.onSetupPhoenixSocket();
    // this.checkTheMarket();

    if(this.state.intervalId) { return false; }
    const that = this;
    const intervalId = setInterval(function() {
      that.checkTheMarket();
    }, 30000);

    this.setState({
      intervalId: intervalId
    })
  }

  componentWillUnmount () {
    clearInterval(this.state.intervalId);
  }

  checkTheMarket = () => {
    const { onCheckTheMarket } = this.props;

    CURRENCIES.map((currency, index) =>  {
      setTimeout(function(){
          onCheckTheMarket(currency);
      }, 200*index);
    })
  }

  render() {
    return (
      <div>
        {React.Children.only(this.props.children)}
      </div>
    );
  }
}

MarketAnalysisProvider.propTypes = {
  dispatch: PropTypes.func.isRequired,
  onCheckTheMarket: PropTypes.func.isRequired,
  onSetupPhoenixSocket: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({

});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onCheckTheMarket: (currency) => dispatch(checkTheMarket(currency)),
    onSetupPhoenixSocket: () => dispatch(setupPhoenixSocket())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'marketAnalysisProvider', reducer });
const withSaga = injectSaga({ key: 'marketAnalysisProvider', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(MarketAnalysisProvider);
