import { select, put } from 'redux-saga/effects';
import { last } from 'lodash';

import { CURRENCIES } from '../constants';
import { makeSelectTradeOpen,
  makeSelectSpecificCurrencyData } from '../selectors';

import { sellTriggered } from '../actions';

function* checkSellTriggerSaga(action) {
  const { ticker } = action;
  const currency = ticker.id;
  const tradeOpen = yield select(makeSelectTradeOpen(currency));

  if(tradeOpen) {
    const currencyData = yield select(makeSelectSpecificCurrencyData(currency));

    if(currencyData) {
      const currentAtr = currencyData.getIn([300, 'atrTrailing']).last().get('down');
      const currentPrice = ticker.last;

      if(currentPrice <= currentAtr) {
        yield put(sellTriggered(currency, new Date().toTimeString(), currentPrice));
      }
    }
  }
}

export default checkSellTriggerSaga;
