import { take, put, call, apply, select } from 'redux-saga/effects'
import { eventChannel, delay } from 'redux-saga'

import { makeSelectTickerChannel } from '../selectors';
import { tickerReceived } from '../actions';
import { CURRENCIES } from '../constants';

// this function creates an event channel from a given socket
// Setup subscription to incoming `ping` events
function createSocketChannel(phoenixChannel) {
  // `eventChannel` takes a subscriber function
  // the subscriber function takes an `emit` argument to put messages onto the channel
  return eventChannel(emit => {

    const pingHandler = (ticker) => {
      // puts event payload into the channel
      // this allows a Saga to take this payload from the returned channel
      emit(ticker)
    }

    // setup the subscription
    phoenixChannel.on('tickerReceived', pl => {
      if(CURRENCIES.includes(pl.ticker.id)) {
        pingHandler(pl.ticker);
      }
    })

    // the subscriber must return an unsubscribe function
    // this will be invoked when the saga calls `channel.close` method
    const unsubscribe = () => {
      // phoenixChannel.off('ping', pingHandler)
    }

    return unsubscribe
  })
}

// reply with a `pong` message by invoking `socket.emit('pong')`
function* pong(socket) {
  yield call(delay, 5000)
  yield apply(socket, socket.emit, ['pong']) // call `emit` as a method with `socket` as context
}

export default function* tickerChannelJoinedSaga() {
  // const socket = yield call(createWebSocketConnection)
  const phoenixChannel = yield select(makeSelectTickerChannel());
  const socketChannel = yield call(createSocketChannel, phoenixChannel)

  while (true) {
    const ticker = yield take(socketChannel)
    yield put(tickerReceived(ticker))
    // yield fork(pong, socket)
  }
}
