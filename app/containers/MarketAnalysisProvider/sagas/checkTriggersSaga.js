import { select } from 'redux-saga/effects';
import { eventChannel, delay } from 'redux-saga'
import { last } from 'lodash';

import { CURRENCIES } from '../constants';
import { makeSelectTradeOpen,
  makeSelectSpecificCurrencyData } from '../selectors';

import { buyTriggered, sellTriggered } from '../actions';

import * as adxAnalyser from '../services/adxAnalyser';
import * as rsi from '../services/rsiAnalyser';
import * as macd from '../services/macdAnalyser';
import * as bollinger from '../services/bbAnalyser';

import { volatilityBreakout } from '../services/indicatorAnalyser';

function analyseStock(currency, currencyData, period) {
  return eventChannel(emit => {

    const triggerHandler = (trigger) => {
      emit(trigger);
    }

    const worker = new Worker("../services/indicatorAnalyser.worker.js");
    worker.postMessage({currency, currencyData, 300});

    worker.onmessage = (event) => {
      triggerHandler(event.data);
    };

    const unsubscribe = () => {
      worker.terminate();
      worker = undefined;
    };
  })
}

function* checkTriggersSaga(action) {
  const currency = action.currency;
  const tradeOpen = yield select(makeSelectTradeOpen(currency));

  if(tradeOpen === false) {
    const currencyData = yield select(makeSelectSpecificCurrencyData(currency));

    if(currencyData) {
      const trigger = yield call(analyseStock, currency, currencyData, 300);

      while (true) {
        const trigegr = yield take(trigger);
        yield put(buyTriggered(currency, new Date().toTimeString()));
      }
    }
  }
}

// function* checkTriggersSaga(action) {
//   const currency = action.currency;
//   const tradeOpen = yield select(makeSelectTradeOpen(currency));
//
//   if(tradeOpen === false) {
//     const currencyData = yield select(makeSelectSpecificCurrencyData(currency));
//
//     if(currencyData && volatilityBreakout(currency, currencyData, 300)) {
//       yield put(buyTriggered(currency, new Date().toTimeString()));
//     }
//   }
// }

export default checkTriggersSaga;
