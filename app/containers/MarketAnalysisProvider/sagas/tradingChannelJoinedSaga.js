import { select, take, put, call } from 'redux-saga/effects';
import { channel } from 'redux-saga'

import { makeSelectTradingChannel } from '../selectors';
import { tradeOpened, tradeClosed } from '../actions';

export default function* tradingChannelJoinedSaga (action) {
  const sagaChannel = yield call(channel);
  const phoenixChannel = yield select(makeSelectTradingChannel());

  phoenixChannel.on('trade_opened', pl => {
    sagaChannel.put(tradeOpened(pl.ticker));
  })

  phoenixChannel.on('trade_closed', pl => {
    sagaChannel.put(tradeClosed(pl.ticker));
  })

  while (true) {
    const action = yield take(sagaChannel)
    yield put(action);
  }
}
