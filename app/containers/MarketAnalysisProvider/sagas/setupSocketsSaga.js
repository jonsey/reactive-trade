import { select, take, fork, put, call } from 'redux-saga/effects';
import { channel } from 'redux-saga'

import { tickerChannelJoined, tradingChannelJoined } from '../actions';

function* setupSocketsSaga (action) {
  var Socket = require("phoenix-socket").Socket;
  const socket = new Socket("ws://localhost:4000/socket");
  socket.connect();

  const sagaChannel = yield call(channel);

  yield fork(joinChannel, sagaChannel, 'ticker:lobby', socket, tickerChannelJoined);
  yield fork(joinChannel, sagaChannel,  'trading:lobby', socket, tradingChannelJoined);

  while (true) {
    const action = yield take(sagaChannel)
    yield put(action);
  }
}

function joinChannel (sagaChannel, topic, socket, phoenixChannelJoinedCallback) {
  const phoenixChannel = socket.channel(topic, {})

  phoenixChannel.join()
    .receive("ok", resp => {
      sagaChannel.put(phoenixChannelJoinedCallback(phoenixChannel));
    })
    .receive("error", resp => { console.log("Unable to join ticker lobby", resp) })
}


export default setupSocketsSaga;
