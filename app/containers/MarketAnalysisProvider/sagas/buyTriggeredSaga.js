import { select } from 'redux-saga/effects';
import { makeSelectTradingChannel } from '../selectors';

function* buyTriggeredSaga (action) {
  const tradingChannel = yield select(makeSelectTradingChannel());
  tradingChannel.push("prediction:buy", {
    "payload": {
      "currency": action.currency
    }
  })
}

export default buyTriggeredSaga;
