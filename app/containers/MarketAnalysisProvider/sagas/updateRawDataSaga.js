import { select, take, put, call } from 'redux-saga/effects';
import { channel } from 'redux-saga'
import { last } from 'lodash';
import { fromJS } from 'immutable';

import { makeSelectSpecificCurrencyData } from '../selectors';
import { updateRawDataForPeriod } from '../actions';

export default function* updateRawDataSaga (action) {
  const sagaChannel = yield call(channel);
  const tickerId = action.ticker.id;
  const currencyData = yield select(makeSelectSpecificCurrencyData(tickerId));

  if (currencyData) {
    try {
      let result = [300].map(period => {
        const rawData = currencyData.getIn([period, 'rawData']);

          const ticker = action.ticker;
          const currentDate = Math.round(Date.now() / 1000);
          const lastCandle = rawData.last();
          const candleDate = lastCandle.get('date');

          if (candleDate < (currentDate - period)) {

            try {
              const newCandle = fromJS({
                open: ticker.last,
                high: ticker.last,
                low: ticker.last,
                close: ticker.last,
                volume: lastCandle.get('baseVolume'), // TODO: not real current volume, need to get from trade socket
                quoteVolume: lastCandle.get('quoteVolume'), // TODO: not real current volume, need to get from trade socket
                date: candleDate + period
              });

              const updatedRawDataForPeriod = rawData.concat([newCandle]);
              sagaChannel.put(updateRawDataForPeriod(tickerId, period, updatedRawDataForPeriod));
            } catch (e) {
              console.log("Error adding raw data", e);
            }

          } else if (candleDate >= (currentDate - period) && candleDate < currentDate) {
              try {
                const updatedCandle = lastCandle
                  .set('close', ticker.last)
                  .set('volume', lastCandle.get('volume')) // TODO: not real current volume, need to get from trade socket
                  .set('quoteVolume', lastCandle.get('quoteVolume')) // TODO: not real current volume, need to get from trade socket
                  .set('high', lastCandle.get('high') < ticker.last ? ticker.last : lastCandle.get('high'))
                  .set('low', lastCandle.get('low') > ticker.last ? ticker.last : lastCandle.get('low'));

                const updatedRawDataForPeriod = rawData.set(rawData.size -1, updatedCandle);
                sagaChannel.put(updateRawDataForPeriod(tickerId, period, updatedRawDataForPeriod));
              } catch (e) {
                console.log("Error updating raw data", e);
              }

          }
      });

      while (true) {
        const action = yield take(sagaChannel)
        yield put(action);
      }
    } catch (e) {
      console.log("Error updating raw data")
    }

  }
}
