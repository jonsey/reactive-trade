import { select } from 'redux-saga/effects';
import { makeSelectTradingChannel } from '../selectors';

function* sellTriggeredSaga (action) {
  const tradingChannel = yield select(makeSelectTradingChannel());
  tradingChannel.push("prediction:sell", {
    "payload": {
      "currency": action.currency,
      "predictions": action.adxData
    }
  })
}

export default sellTriggeredSaga;
