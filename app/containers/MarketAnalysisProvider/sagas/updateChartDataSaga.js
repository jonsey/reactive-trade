import { select, take, put, call } from 'redux-saga/effects';
import { channel } from 'redux-saga'
import { last } from 'lodash';

import { makeSelectSpecificCurrencyData } from '../selectors';
import { updateRawData,
  updateRawDataForPeriod,
  updateChartDataForPeriod,
  checkTheMarket } from '../actions';


async function updateChartData (rawData) {
  const adxData = await techan.indicator.adx()(rawData);
  const rsiData = await techan.indicator.rsi()(rawData);
  const macdData = await techan.indicator.macd()(rawData);
  const bb = await techan.indicator.bollinger()(rawData);

  return { rawData, adxData, rsiData, macdData, bb };
}

export default function* updateChartDataSaga (action) {
  const sagaChannel = yield call(channel);
  const tickerId = action.ticker.id;
  const currencyData = yield select(makeSelectSpecificCurrencyData(tickerId));

  if (currencyData) {
    try {
      let result = [300, 900, 1800, 7200].map(period => {
        const rawData = currencyData.getIn([period, 'rawData']);
        // return currencyData.updateIn([period, 'rawData'], rawData => {
          // debugger
          const ticker = action.ticker;
          const currentDate = Math.round(Date.now() / 1000);
          const lastCandle = rawData.last();
          const candleDate = lastCandle.get('date');

          if (candleDate < (currentDate - period)) {
            // sagaChannel.put(checkTheMarket(ticker.id));
            const newCandle = {
              open: ticker.last,
              high: ticker.last,
              low: ticker.last,
              close: ticker.last,
              volume: lastCandle.get('baseVolume'), // TODO: not real current volume, need to get from trade socket
              quoteVolume: lastCandle.get('quoteVolume'), // TODO: not real current volume, need to get from trade socket
              date: candleDate + period
            };

            const updatedChartData = updateChartData(rawData.concat([newCandle]).toJS());
            updatedChartData.then(x => sagaChannel.put(updateChartDataForPeriod(tickerId, period, x)));
          } else if (candleDate >= (currentDate - period) && candleDate < currentDate) {
              let updatedCandle;
              updatedCandle = lastCandle
                .set('close', ticker.last)
                .set('volume', lastCandle.get('volume')) // TODO: not real current volume, need to get from trade socket
                .set('quoteVolume', lastCandle.get('quoteVolume')) // TODO: not real current volume, need to get from trade socket
                .set('high', lastCandle.get('high') < ticker.last ? ticker.last : lastCandle.get('high'))
                .set('low', lastCandle.get('low') > ticker.last ? ticker.last : lastCandle.get('low'));

              const updatedRawDataForPeriod = rawData.set(rawData.size -1, updatedCandle);
              const updatedChartData = updateChartData(updatedRawDataForPeriod.toJS());
              updatedChartData.then(x => sagaChannel.put(updateChartDataForPeriod(tickerId, period, x)));
              // sagaChannel.put(updateRawDataForPeriod(tickerId, period, updatedRawDataForPeriod));
          }
        // });
      });

      // debugger;
      // yield put(updateRawData(tickerId, result[0]));

      while (true) {
        const action = yield take(sagaChannel)
        yield put(action);
      }
    } catch (e) {
      console.log("Error updating raw data")
    }

  }
}
