import { select, put, call } from 'redux-saga/effects';
import {  } from '../selectors';
import {
  chartDataLoadingError,
  marketDataLoaded
} from '../actions';

import { fetchChartData } from '../../../services/exchange_api';

export function* getChartDataSaga(args) {
  const { currency } = args;

  try {
    // const periods = [300, 900, 1800, 7200];
    const periods = [300];


    for (var i = 0; i < periods.length; i++) {
      const period = periods[i];
      let startDifference = 60 * 60 * 1000;

      switch (period) {
        case 300:
          startDifference = 12 * startDifference;
          break;
        case 900:
          startDifference = 2 * 24 * startDifference;
          break;
        case 1800:
          startDifference = 2.5 * 24 * startDifference;
          break;
        case 7200:
          startDifference = 10 * 24 * startDifference;
          break;
      }

      const start = Math.trunc(new Date(new Date().getTime() - startDifference).getTime() / 1000);

      const marketDataItems = yield call(fetchChartData, currency, start, period);

      yield put(marketDataLoaded(marketDataItems.data,
        marketDataItems.adxData,
        marketDataItems.rsiData,
        marketDataItems.macdData,
        marketDataItems.bb,
        marketDataItems.atrTrailing,
        marketDataItems.currency,        
        period));
    }

  } catch (err) {
    console.log("Error", err)
    yield put(chartDataLoadingError(err));
  }
}

export default getChartDataSaga;
