/*
 *
 * MarketAnalysisProvider actions
 *
 */

import {
  DEFAULT_ACTION,
  SETUP_PHOENIX_SOCKET,
  TRADING_CHANNEL_JOINED,
  TICKER_CHANNEL_JOINED,
  CURRENCY_CHANGE,
  CHECK_THE_MARKET,
  CHART_DATA_LOADED,
  MARKET_DATA_LOADED,
  BUY_TRIGGERED,
  SELL_TRIGGERED,
  TRADE_OPENED,
  TRADE_CLOSED,
  TICKER_RECIEVED,
  CHART_DATA_LOADING_ERROR,
  UPDATE_RAW_DATA,
  UPDATE_CURRENCY_PERIOD_CHART_DATA,
  UPDATE_CURRENCY_PERIOD_RAW_DATA
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function setupPhoenixSocket() {
  return {
    type: SETUP_PHOENIX_SOCKET
  }
}

export function tradingChannelJoined(channel) {
  return {
    type: TRADING_CHANNEL_JOINED,
    channel
  }
}

export function tickerChannelJoined(channel) {
  return {
    type: TICKER_CHANNEL_JOINED,
    channel
  }
}

export function tickerReceived (ticker) {
  return {
    type: TICKER_RECIEVED,
    ticker
  }
}

export function tradeOpened(trade) {
  return {
    type: TRADE_OPENED,
    trade
  }
}

export function tradeClosed(trade) {
  return {
    type: TRADE_CLOSED,
    trade
  }
}

export function currencyChange(currency) {
  return {
    type: CURRENCY_CHANGE,
    currency,
  };
}

export function checkTheMarket(currency) {
  return {
    type: CHECK_THE_MARKET,
    currency
  };
}

export function marketDataLoaded(data, adxData, rsiData, macdData, bb, atrTrailing, currency, period) {
  return {
    type: MARKET_DATA_LOADED,
    data,
    adxData,
    rsiData,
    macdData,
    bb,
    atrTrailing,
    currency,
    period
  }
}

export function chartDataLoadingError(err) {
  return {
    type: CHART_DATA_LOADING_ERROR,
    err,
  };
}

export function buyTriggered(currency, date, price) {
  return {
    type: BUY_TRIGGERED,
    currency,
    date,
    price
  };
}

export function sellTriggered(currency, date, price) {
  return {
    type: SELL_TRIGGERED,
    currency,
    date,
    price
  };
}

export function updateRawData(currency, rawData) {
  return {
    type: UPDATE_RAW_DATA,
    currency,
    rawData
  }
}

export function updateChartDataForPeriod(currency, period, chartData) {
  return {
    type: UPDATE_CURRENCY_PERIOD_CHART_DATA,
    currency,
    period,
    chartData
  }
}

export function updateRawDataForPeriod(currency, period, rawData) {
  return {
    type: UPDATE_CURRENCY_PERIOD_RAW_DATA,
    currency,
    period,
    rawData
  }
}
