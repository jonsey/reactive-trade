import { last, takeRight } from 'lodash';

const MINUS_DI_INCREASE_PERCENTAGE = 1;

export const analyseBuySignal = (data) => {
  // console.log("adx Data", data);
  const lastItem = data.slice(-1)[0];

  return plusDiIncreasingOverPeriod(data, 2)
    && plusDiIsGreaterThanMinusDi(lastItem)
    && trendIsGreaterThanThreshold(lastItem, 25)
    && minusDiIsGreaterThanThreshold(last(data), 20) === false
    // && positiveCrossedTheNegative(data)
}

export const minusDiGreaterThanPlusDi = (data) => {
  const item = last(data);
  return item.plusDi < item.minusDi;
}

const plusDiIncreasingOverPeriod = (data, period) => {
  const periodData = data.slice(-period);
  let lastValue = 0;
  let result = false;

  for (var i = 0; i < periodData.length; i++) {
    const datum = periodData[i]

    if(datum.plusDi > lastValue) {
      lastValue = datum.plusDi;
      result = true;
    } else {
      result = false;
      break;
    }
  }

  return result;
}

const plusDiIsGreaterThanMinusDi = (data) => {
  return data.plusDi > data.minusDi;
}

const trendIsGreaterThanThreshold = (data, threshold) => {
  return data.adx > threshold;
}

const positiveCrossedTheNegative = (data) => {
  const lastTwoPoints = data.slice(-2);
  return lastTwoPoints[0].plusDi <= lastTwoPoints[0].minusDi && lastTwoPoints[1].minusDi < lastTwoPoints[1].plusDi
}

export const analyseSellSignal = (data) => {
  return minusDiIncreasing(data, 2) && minusDiIsGreaterThanThreshold(last(data), 20)
}

export const minusDiIncreasing = (data, period = 2) => {
  const periodData = data.slice(-period);
  let lastValue = 0;
  let result = false;

  for (var i = 0; i < periodData.length; i++) {
    const datum = periodData[i]

    if(datum.minusDi > lastValue) {
      lastValue = datum.minusDi;
      result = true;
      break;
    } else {
      result = false;
    }
  }

  return result;
}

export const minusDiIsGreaterThanThreshold = (data, threshold = 20) => {
  return data.minusDi > threshold;
}
