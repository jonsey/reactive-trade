
const CROSS_OVER_PERCENTAGE = 0.05;

import { takeRight, last } from 'lodash';

export const macdMovedAboveZero = (data) => {
  const items = takeRight(data, 2);
  return items[0].macd <= 0 && items[1].macd > 0;
}

export const macdSignalCrossedOverZero = (data) => {
  const items = takeRight(data, 2);
  return items[0].signal <= 0 && items[1].signal > 0;
}

export const macdSignalPositive = (data) => {
  const item = data.last();
  return item.get('signal') > 0;
}

// export const macdSignalCrossedOverMacd = (data) => {
//   const items = takeRight(data, 2);
//   return items[0].signal <= items[0].macd
//     && items[1].signal > items[1].macd;
// }
//
// export const macdMacdCrossedBelowSignal = (data) => {
//   const items = takeRight(data, 2);
//   return items[0].signal <= items[0].macd
//     && items[1].signal > items[1].macd;
// }

export const histogramBecamePositive = (data) => {
  const items = takeRight(data, 2);
  return items[0].difference <= 0
    && items[1].difference > 0
}

export const histogramBecameNegative = (data) => {
  const items = takeRight(data, 2);
  return items[0].difference > 0
    && items[1].difference < 0
}
