import { takeRight, last } from 'lodash';

export const priceCrossedMiddleLine = (rawData, bbData) => {
  const candles = takeRight(rawData, 2);
  const bb = takeRight(bbData, 2);

  return candleIsGreen(candles[1])
    && candleIsAboveMiddleLine(candles[1], bb[1])
    && candleIsBelowMiddleLine(candles[0], bb[0])
}

export const currentWidth = (data) => {
  const bb = last(data);
  return itemWidth(bb);
}

export const itemWidth = (bb) => {
  const upperBand = bb.get('upperBand');
  const lowerBand = bb.get('lowerBand');
  const middleBand = bb.get('middleBand');
  return ((upperBand - lowerBand) / middleBand) * 100;
}


export const percentB = (rawData, data) => {
  const bb = last(data);
  const candle = last(rawData);
  return (candle.close - bb.lowerBand) / (bb.upperBand - bb.lowerBand);
}

export const isAboveUpperBand = (rawData, data) => {
  const candle = last(rawData);
  const band = last(data);
  return candle.close > band.upperBand;
}

export const middleBandIsIncreasing = (data) => {
  const bands = data.takeLast(2);
  const isIncreasing = getMiddleBand(1, bands) > getMiddleBand(0, bands);
  return isIncreasing;
}

const  getMiddleBand = (index, bands) => {
  return bands.getIn([index, 'middleBand']);
}

export const movedInsideLowerband = (rawData, bbData) => {
  const candles = takeRight(rawData, 2);
  const bb = takeRight(bbData, 2);
  return hasMovedInsideLowerBand(candles[1], candles[0], bb[1], bb[0])
}

export const retracingOutsideUpperBand = (rawData, bbData) => {
  const candle = last(rawData);
  const bb = last(bbData);
  return candleIsGreen(candle) === false
    && isOutsideUpperBand(candle, bb);
}

const hasMovedInsideLowerBand = (currentCandle, lastCandle, currentBB, lastBB) => {
  return candleIsOutsideBB(lastCandle, lastBB)
    && candleIsInsideLowerBand(currentCandle, currentBB)
    && candleIsGreen(currentCandle);
}

const isOutsideUpperBand = (candle, bb) => {
  return candle.close > bb.upperBand;
}

const candleIsGreen = (candle) => {
  return candle.open < candle.close;
}

const candleIsAboveMiddleLine = (candle, bb) => {
  return candle.close > bb.middleBand;
}

const candleIsBelowMiddleLine = (candle, bb) => {
  return candle.close < bb.middleBand;
}

const candleIsInsideLowerBand = (candle, bb) => {
  return candle.close > bb.lowerBand;
}

const candleIsOutsideBB = (candle, bb) => {
  return candle.close < bb.lowerBand;
}
