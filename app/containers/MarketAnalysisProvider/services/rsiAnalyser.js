import { last } from 'lodash';

export const isOverSold = (data) => {
  const item = _.last(data);
  return item.rsi < 30;
}

export const isOverBought = (data) => {
  const item = _.last(data);
  return item.rsi > 70;
}
