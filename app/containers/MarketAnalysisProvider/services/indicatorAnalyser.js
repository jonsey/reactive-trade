import { last, takeRight, take } from 'lodash';

import * as adxAnalyser from '../services/adxAnalyser';
import * as rsi from '../services/rsiAnalyser';
import * as macd from '../services/macdAnalyser';
import * as bollinger from '../services/bbAnalyser';

import { CURRENCIES } from '../constants';


export const overSold = (rsiData) => rsi.isOverSold(rsiData);

export const macdSignalCrossedOverZero = (macdData) =>  macd.macdSignalCrossedOverZero(macdData);
export const macdSignalCrossedOverMacd = (macdData) =>  macd.macdSignalCrossedOverMacd(macdData);
export const macdHistogramBecamePositive = (macdData) =>  macd.histogramBecamePositive(macdData);
export const macdHistogramBecameNegative = (macdData) =>  macd.histogramBecameNegative(macdData);
export const macdMacdCrossedBelowSignal = (macdData) => macd.macdMacdCrossedBelowSignal(macdData);
export const macdSignalPositive = (macdData) => macd.macdSignalPositive(macdData);

export const priceCrossedMiddleBB = (data, bb) =>  bollinger.priceCrossedMiddleLine(data, bb);
export const movedInsideLowerBB = (data, bb) =>  bollinger.movedInsideLowerband(data, bb);
export const retracingOutsideUpperBand = (data, bb) =>  bollinger.retracingOutsideUpperBand(data, bb);
export const isAboveUpperBand = (data, bb) =>  bollinger.isAboveUpperBand(data, bb);
export const middleBandIsIncreasing = (bb) =>  bollinger.middleBandIsIncreasing(bb);
export const bbWidth = (bb) => bollinger.currentWidth(bb);
export const bbItemWidth = (item) => bollinger.itemWidth(item);

export const minusDiGreaterThanPlusDi = (adxData) => adxAnalyser.minusDiGreaterThanPlusDi(adxData);
export const minusDiIncreasing = (adxData) =>  adxAnalyser.minusDiIncreasing(adxData);
export const minusDiIsGreaterThanThreshold = (adxData) =>  adxAnalyser.minusDiIsGreaterThanThreshold(last(adxData), 10);


export const volatilityBreakout = (currency, currencyData, period) => {
  const periodData = currencyData.get(period);
  const bbData = periodData.get('bb');
  const macdData = periodData.get('macdData');

  if(currency === 'BTC_VRC' && period === 300) {
    console.log("er")
  }

  const fnMacdSigPositive = () => macdSignalPositive(macdData);
  const fnEmaIncreasing = () => middleBandIsIncreasing(bbData);
  const fnSqueezeExpand = () => squeezeAndExpand(currency, period, bbData, 15, 2, 0.25);

  return fnSqueezeExpand() && fnEmaIncreasing() && fnMacdSigPositive();
}

const squeezeAndExpand = (currency, period, bandData, lookBackPeriod, expansionPeriod, expansionPercentage) => {
  const expansionData = bandData.takeLast(expansionPeriod);
  const squeezeData = bandData.take(bandData.size - expansionPeriod);
  const lookBackData = squeezeData.takeLast(lookBackPeriod);
  const sumOfWidth = bandData.reduce((acc, item) => {
    return acc + bbItemWidth(item)
  }, 0);
  const avgWidth = sumOfWidth / bandData.size;

  // console.log(`${currency} Avg Width: ${avgWidth} over ${(period/60)*bandData.length} mins`)

  const fnSqueeze = () => squeezeWidthBelowAverage(lookBackData, avgWidth);
  const fnExpanding = () => expansionDataIncreasing(expansionData);
  const fnExpandingAboveAvg = () => expansionDataWidthAboveAverage(expansionData, avgWidth, expansionPercentage)

  return fnSqueeze() && fnExpandingAboveAvg() && fnExpanding();
}

const squeezeWidthBelowAverage = (lookBackData, avgWidth) => {
  for (var i = 0; i < lookBackData.size; i++) {
    const item = lookBackData.get(i);

    if (bbItemWidth(item) > avgWidth) {
      return false;
    }
  }

  return true;
}

const expansionDataWidthAboveAverage = (expansionData, avgWidth, expansionPercentage) => {
  return bbItemWidth(expansionData.last()) > avgWidth * (1 + expansionPercentage)
}

const expansionDataIncreasing = (expansionData) => {
  let lastBandWidth = bbItemWidth(expansionData.get(0));

  for (var i = 1; i < expansionData.size; i++) {
    const item = expansionData.get(i);
    const currentWidth = bbItemWidth(item)

    if (currentWidth > lastBandWidth) {
      lastBandWidth = bbItemWidth(item)
    } else {
      return false;
    }
  }

  return true;
}

export const analyseCurrency = (currencyData) => {
  let allPeriodsBBIncreasing = true;
  currencyData.entrySeq().forEach(e => {
    const period = e[0];
    const data = e[1];
    if(period !== 300) {
      if (middleBandIsIncreasing(data.get('bb')) === false) {
        allPeriodsBBIncreasing = false
      }
    }
  });

  return allPeriodsBBIncreasing;
}
