import { volatilityBreakout } from 'indicatorAnalyser';

onmessage = (e) => {
  const { currency, currencyData, period } = e.data;
  const trigger = volatilityBreakout(currency, currencyData, period);

  postMessage({
    type: 'trigger',
    value: trigger
  });
};
