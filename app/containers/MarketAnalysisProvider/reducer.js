/*
 *
 * MarketAnalysisProvider reducer
 *
 */


import { fromJS } from 'immutable';
import { last } from 'lodash';
import {
  DEFAULT_ACTION,
  SETUP_PHOENIX_SOCKET,
  TRADING_CHANNEL_JOINED,
  TICKER_CHANNEL_JOINED,
  CURRENCY_CHANGE,
  CHECK_THE_MARKET,
  CHART_DATA_LOADED,
  MARKET_DATA_LOADED,
  BUY_TRIGGERED,
  SELL_TRIGGERED,
  TRADE_OPENED,
  TRADE_CLOSED,
  TICKER_RECIEVED,
  CHART_DATA_LOADING_ERROR,
  UPDATE_RAW_DATA,
  UPDATE_CURRENCY_PERIOD_CHART_DATA,
  UPDATE_CURRENCY_PERIOD_RAW_DATA
} from './constants';

import { updateChartDataService } from '../../services/updateChartDataService';

const initialState = fromJS({
  tradingChannel: null,
  tickerChannel: null,
  loading: false,
  error: false,
  triggers: [],
  trades: [],
  openedTrades: [],
  openTrades: [],
  closedTrades: [],
  currencyData: {},
  tickerData: {},
  currency: '',
  showTriggers: false
});

const mapTrade = (trade, type) => {
  return {
    currency: trade.currency_pair,
    type: type,
    date: trade.date,
    price: trade.rate,
    amount: trade.amount,
    profit: null
  }
}

const mapClosingTrade = (state, trade) => {
  let profit = 0;
  const currency = trade.currency_pair.replace('BTC_', '')
  const currencyData = state.get('currencyData').get(currency);

  if( currencyData ) {
    const openingTrades = state.get('openTrades').filter(x => x.currency === trade.currency_pair);
    const openingTradesValue = openingTrades.reduce((total, trade) => total + (trade.amount * trade.price), 0)

    const closingValue = trade.rate * trade.amount;

    profit = (closingValue - openingTradesValue) * 5000;
  }

  return {
    currency: trade.currency_pair,
    date: trade.date,
    type: "SELL",
    price: trade.rate,
    amount: trade.amount,
    profit: profit
  }
}

function marketAnalysisProviderReducer(state = initialState, action) {
  switch (action.type) {
    case CURRENCY_CHANGE:
      return state
        .set('currency', action.currency);

    case CHECK_THE_MARKET:
      return state.set('showTriggers', false);

    case SETUP_PHOENIX_SOCKET:
      return state;

    case TRADING_CHANNEL_JOINED:
      return state
        .set('tradingChannel', action.channel);

    case TICKER_CHANNEL_JOINED:
      return state
        .set('tickerChannel', action.channel);

    case MARKET_DATA_LOADED:
      return state
        .set('showTriggers', true)
        .updateIn(['currencyData'], (list) => {
            return list
              .setIn([action.currency, action.period, 'currentPrice'], last(action.data).close)
              .setIn([action.currency, action.period, 'rawData'], fromJS(action.data))
              .setIn([action.currency, action.period, 'adxData'], fromJS(action.adxData))
              .setIn([action.currency, action.period, 'macdData'], fromJS(action.macdData))
              .setIn([action.currency, action.period, 'rsiData'], fromJS(action.rsiData))
              .setIn([action.currency, action.period, 'bb'], fromJS(action.bb))
              .setIn([action.currency, action.period, 'atrTrailing'], fromJS(action.atrTrailing));
        });

    case UPDATE_RAW_DATA:
      return state
        .setIn(['currencyData', action.currency], fromJS(action.rawData));

    case UPDATE_CURRENCY_PERIOD_RAW_DATA:
      return state
        .setIn(['currencyData', action.currency, action.period, 'rawData'], action.rawData);

    case UPDATE_CURRENCY_PERIOD_CHART_DATA:
      return state
        .setIn(['currencyData', action.currency, action.period], fromJS(action.chartData));

    case BUY_TRIGGERED:
      return state
        .set('showTriggers', true)
        .update('triggers', (list) => {
          if(list.size > 100) {
            list = list.splice(-100);
          }
          return list.push({
            currency: action.currency,
            type: 'BUY',
            date: action.date,
            price: action.price
          })
        });

    case SELL_TRIGGERED:
      return state
        .set('showTriggers', true)
        .update('triggers', (list) => {
          if(list.size > 100) {
            list = list.splice(-100);
          }
          return list.push({
            currency: action.currency,
            type: 'SELL',
            date: action.date,
            price: action.price
          })
        });

    case TRADE_OPENED:
      const openingTrade = mapTrade(action.trade, "BUY");
      return state
        .update('openedTrades', (list) => list.push(openingTrade))
        .update('openTrades', (list) => list.push(openingTrade))
        .update('trades', (list) => list.push(openingTrade));

    case TRADE_CLOSED:
      const closingTrade = mapClosingTrade(state, action.trade);
      return state
        .update('closedTrades', (list) => list.push(closingTrade))
        .update('openTrades', (list) => list.filter(trade => trade.currency !== closingTrade.currency))
        .update('trades', (list) => list.push(closingTrade));

    case CHART_DATA_LOADING_ERROR:
      return state;

    case DEFAULT_ACTION:
      return state;

    default:
      return state;
  }
}

export default marketAnalysisProviderReducer;
