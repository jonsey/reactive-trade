
import { fromJS } from 'immutable';
import marketAnalysisProviderReducer from '../reducer';

describe('marketAnalysisProviderReducer', () => {
  it('returns the initial state', () => {
    expect(marketAnalysisProviderReducer(undefined, {})).toEqual(fromJS({}));
  });
});
