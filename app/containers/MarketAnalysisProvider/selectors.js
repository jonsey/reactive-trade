import { createSelector } from 'reselect';

/**
 * Direct selector to the marketAnalysisProvider state domain
 */
const selectMarketAnalysisProviderDomain = (state) => state.get('marketAnalysisProvider');

export const makeSelectTradingChannel = () => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => substate.get('tradingChannel')
);

export const makeSelectTickerChannel = () => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => substate.get('tickerChannel')
);

export const makeSelectCurrency = () => createSelector(
   selectMarketAnalysisProviderDomain,
   (substate) => substate.get('currency')
 );

export const makeSelectCurrencyData = () => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => substate.get('currencyData')
);

export const makeSelectSpecificCurrencyData = (currency) => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => {
    if(substate.getIn(['currencyData', currency])) {
      return substate.getIn(['currencyData', currency])
    } else {
      return false;
    }

  }
);

export const makeSelectShowTriggers = () => createSelector(
 selectMarketAnalysisProviderDomain,
 (substate) => substate.get('showTriggers')
);

export const makeSelectTrades = () => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => substate.get('trades')
);

export const makeSelectCurrencyTrades = (currency) => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => substate.get('trades').filter(trade => trade.currency === currency)
);


export const makeSelectTriggers = () => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => substate.get('triggers')
);

export const makeSelectOpenTrades = () => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => substate.get('openTrades')
);

export const makeSelectOpenedTrades = () => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => substate.get('openedTrades')
);

export const makeSelectClosedTrades = () => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => substate.get('closedTrades')
);

export const makeSelectTradeOpen = (currency) => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => {
    const trades = substate.get('openTrades').filter(x => x.currency === `BTC_${currency}`);
    return trades.size > 0;
  }
);

const makeSelectMarketAnalysisProvider = () => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => substate.toJS()
);

export default makeSelectMarketAnalysisProvider;
export {
  selectMarketAnalysisProviderDomain,
};
