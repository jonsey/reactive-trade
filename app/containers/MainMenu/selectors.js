import { createSelector } from 'reselect';

/**
 * Direct selector to the mainMenu state domain
 */
const selectMainMenuDomain = (state) => state.get('mainMenu');

/**
 * Other specific selectors
 */


/**
 * Default selector used by MainMenu
 */

const makeSelectMainMenu = () => createSelector(
  selectMainMenuDomain,
  (substate) => substate.toJS()
);

export default makeSelectMainMenu;
export {
  selectMainMenuDomain,
};
