/**
 *
 * MainMenu
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import RemoveRedEye from 'material-ui/svg-icons/image/remove-red-eye';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import ContentLink from 'material-ui/svg-icons/content/link';
import Divider from 'material-ui/Divider';
import ContentCopy from 'material-ui/svg-icons/content/content-copy';
import Delete from 'material-ui/svg-icons/action/delete';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectMainMenu from './selectors';
import reducer from './reducer';
import saga from './saga';

const style = {
  paper: {
    display: 'inline-block',
    float: 'left',
    margin: '16px 32px 16px 0',
  },
  rightIcon: {
    textAlign: 'center',
    lineHeight: '24px',
  },
};

export class MainMenu extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Paper style={style.paper}>
          <Menu>
            <MenuItem primaryText="Live Trades" leftIcon={<RemoveRedEye />} onClick={this.props.onTrades} />
            <MenuItem primaryText="Market Analysis" leftIcon={<PersonAdd />} onClick={this.props.onMarketAnalysis} />
            <MenuItem primaryText="Instrument Analysis" leftIcon={<ContentLink />} onClick={this.props.onInstrumentAnalysis} />
            <Divider />
            <MenuItem primaryText="Manual Trading" leftIcon={<ContentCopy />}  onClick={this.props.onManualTrading} />
            <Divider />
            <MenuItem primaryText="Panik" leftIcon={<Delete />} />
          </Menu>
        </Paper>
      </div>
    );
  }
}

MainMenu.propTypes = {
  onTrades: PropTypes.func,
  onMarketAnalysis: PropTypes.func,
  onInstrumentAnalysis: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  mainmenu: makeSelectMainMenu(),
});

function mapDispatchToProps(dispatch) {
  return {
    onTrades: () => dispatch(push('/trades')),
    onMarketAnalysis: () => dispatch(push('/market-analysis')),
    onInstrumentAnalysis: () => dispatch(push('/instrument')),
    onManualTrading: () => dispatch(push('/trading'))
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'mainMenu', reducer });
const withSaga = injectSaga({ key: 'mainMenu', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(MainMenu);
