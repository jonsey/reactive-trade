/*
 * MainMenu Messages
 *
 * This contains all the text for the MainMenu component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.MainMenu.header',
    defaultMessage: 'This is MainMenu container !',
  },
});
