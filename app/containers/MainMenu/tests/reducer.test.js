
import { fromJS } from 'immutable';
import mainMenuReducer from '../reducer';

describe('mainMenuReducer', () => {
  it('returns the initial state', () => {
    expect(mainMenuReducer(undefined, {})).toEqual(fromJS({}));
  });
});
