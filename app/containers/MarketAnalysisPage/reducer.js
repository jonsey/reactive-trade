/*
 *
 * MarketAnalysisPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  BUY_TRIGGERED,
  SELL_TRIGGERED
} from './constants';

const initialState = fromJS({
  loading: false,
  error: false,
  showTriggers: false
});

function marketAnalysisPageReducer(state = initialState, action) {
  switch (action.type) {

    default:
      return state;
  }
}

export default marketAnalysisPageReducer;
