/**
 *
 * MarketAnalysisPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import {Tabs, Tab} from 'material-ui/Tabs';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import Adxgraph from 'components/Adxgraph/Loadable';
import MarketTriggers from 'components/MarketTriggers';

import {
  makeSelectTriggers,
  makeSelectShowTriggers } from 'containers/MarketAnalysisProvider/selectors';

import reducer from './reducer';
import saga from './saga';
import messages from './messages';

const style = {
  margin: 12,
};

class MarketAnalysisPage extends React.PureComponent {
  constructor(props) {
    super(props);


  }

  render() {
    const { showTriggers, currencyData, triggers } = this.props;

    return (
      <div>
        <h2>Market Data</h2>
        <MarketTriggers triggers={ triggers } />
      </div>
    );
  }
}

MarketAnalysisPage.propTypes = {
  triggers: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  triggers: makeSelectTriggers(),
  showTriggers: makeSelectShowTriggers()
});

function mapDispatchToProps(dispatch) {
  return {

  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'marketanalysispage', reducer });
const withSaga = injectSaga({ key: 'marketanalysispage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(MarketAnalysisPage);
