
import { fromJS } from 'immutable';
import marketAnalysisPageReducer from '../reducer';

describe('marketAnalysisPageReducer', () => {
  const initialState = fromJS({
    loading: false,
    error: false,
    currencyData: [{
      currency: '',
      rawData: [],
      indicators: {
        adx: [],
        rsi: [],
      },
    }],
    currency: '',
    data: [],
  });

  it('returns the initial state', () => {
    expect(marketAnalysisPageReducer(undefined, {})).toEqual(initialState);
  });
});
