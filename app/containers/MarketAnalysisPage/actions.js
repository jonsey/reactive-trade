/*
 *
 * MarketAnalysisPage actions
 *
 */

import {
  BUY_TRIGGERED,
  SELL_TRIGGERED,
} from './constants';

export function buyTriggered(data, adxData, rsiData, macdData, currency) {
  return {
    type: BUY_TRIGGERED,
    data,
    adxData,
    rsiData,
    macdData,
    currency,
  };
}

export function sellTriggered(data, adxData, rsiData, macdData, currency) {
  return {
    type: SELL_TRIGGERED,
    data,
    adxData,
    rsiData,
    macdData,
    currency,
  };
}
