import { createSelector } from 'reselect';

/**
 * Direct selector to the marketAnalysisPage state domain
 */
const selectMarketAnalysisPageDomain = (state) => state.get('marketanalysispage');
const selectMarketAnalysisProviderDomain = (state) => state.get('marketanalysisprovider');

export const makeSelectShowTriggers = () => createSelector(
 selectMarketAnalysisPageDomain,
 (substate) => substate.get('showTriggers')
);

export const makeSelectTriggers = () => createSelector(
  selectMarketAnalysisProviderDomain,
  (substate) => substate.get('triggers')
);
