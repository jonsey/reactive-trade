/*
 * MarketAnalysisPage Messages
 *
 * This contains all the text for the MarketAnalysisPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.MarketAnalysisPage.header',
    defaultMessage: 'Market Analysis !',
  },
});
