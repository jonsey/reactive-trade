/*
 *
 * MarketAnalysisPage constants
 *
 */

export const BUY_TRIGGERED = 'app/MarketAnalysisPage/BUY_TRIGGERED';
export const SELL_TRIGGERED = 'app/MarketAnalysisPage/SELL_TRIGGERED';
