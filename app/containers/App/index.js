/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import styled from 'styled-components';

import MainMenu from 'containers/MainMenu/Loadable';

import HomePage from 'containers/HomePage/Loadable';
import TradesPage from 'containers/TradesPage/Loadable';
import MarketAnalysisPage from 'containers/MarketAnalysisPage/Loadable';
import InstrumentPage from 'containers/InstrumentPage/Loadable';
import TradingPage from 'containers/TradingPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

const Wrapper = styled.div `
  margin-left: 25%;
`;


export default function App() {
  return (
    <div>
      <MainMenu />
      <Wrapper>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/trades" component={TradesPage} />
          <Route path="/market-analysis" component={MarketAnalysisPage} />
          <Route path="/instrument" component={InstrumentPage} />
          <Route path="/trading" component={TradingPage} />
          <Route component={NotFoundPage} />
        </Switch>
      </Wrapper>

    </div>
  );
}
