import { takeLatest, call, put, select } from 'redux-saga/effects';

import { SET_CURRENCY } from './constants';
import { getChartData } from './actions';

function* doGetChartData (action) {
  yield put(getChartData(action.currency));
}

// Individual exports for testing
export default function* defaultSaga() {
  yield takeLatest(SET_CURRENCY, doGetChartData);
}
