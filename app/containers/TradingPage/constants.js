/*
 *
 * TradingPage constants
 *
 */

export const DEFAULT_ACTION = 'app/TradingPage/DEFAULT_ACTION';
export const SET_CURRENCY = 'app/TradingPage/SET_CURRENCY';
