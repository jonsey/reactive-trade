/*
 *
 * TradingPage actions
 *
 */

import {
  DEFAULT_ACTION,
  SET_CURRENCY
} from './constants';

import { CHECK_THE_MARKET } from '../MarketAnalysisProvider/constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getChartData (currency) {
  return {
    type: CHECK_THE_MARKET,
    currency
  }
}

export function setCurrency (currency) {
  return {
    type: SET_CURRENCY,
    currency
  }
}
