/*
 *
 * TradingPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  SET_CURRENCY
} from './constants';

const initialState = fromJS({
  currency: ''
});

function tradingPageReducer(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENCY:
      return state.set('currency', action.currency);
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default tradingPageReducer;
