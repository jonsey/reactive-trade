/**
 *
 * TradingPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import styled from 'styled-components';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import TradingToolbar from 'components/TradingToolbar/Loadable'

import { makeSelectChartData,
  makeSelectTickerData,
  makeSelectTradeHistory,
  makeSelectAsks,
  makeSelectBids
} from './selectors';
import { setCurrency } from './actions';
import reducer from './reducer';
import saga from './saga';

import FullAnalysisGraph from 'components/FullAnalysisGraph';

const TradingToolbarWrapper = styled.div `
  position: fixed;
  padding-top: 1em;
  width: 70%;
`;

const MainBody = styled.div `
  padding-top: 5em;
`;


export class TradingPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor (props) {
    super(props);

    this.state = ({
      currency: 'BTC_VTC'
    })
  }

  componentWillReceiveProps (np) {

  }

  onCurrencyChange = (e) => {
    this.setState({
      currency: e.target.value
    })
  }

  render() {
    const { chartData, tradeHistory, onSetCurrency } = this.props;
    const { currency } = this.state;

    return (
      <div>
        <TradingToolbarWrapper>
          <TradingToolbar onSetCurrency={ onSetCurrency } />
        </TradingToolbarWrapper>

        <MainBody>
          { chartData &&
            (<div>
              <FullAnalysisGraph data={ chartData.get(7200) } trades={ tradeHistory } period={7200} currency={ currency } />
              <FullAnalysisGraph data={ chartData.get(1800) } trades={ tradeHistory } period={1800} currency={ currency } />
              <FullAnalysisGraph data={ chartData.get(900) } trades={ tradeHistory } period={900} currency={ currency } />
              <FullAnalysisGraph data={ chartData.get(300) } trades={ tradeHistory } period={300} currency={ currency } />
            </div>)
          }
        </MainBody>
      </div>

    );
  }
}

TradingPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  onSetCurrency: PropTypes.func.isRequired,
  chartData: PropTypes.object,
  tickerData: PropTypes.object,
  tradeHistory: PropTypes.object,
  asks: PropTypes.object,
  bids: PropTypes.object
};



const mapStateToProps = createStructuredSelector({
  chartData: makeSelectChartData(),
  tickerData: makeSelectTickerData(),
  tradeHistory: makeSelectTradeHistory(),
  asks: makeSelectAsks(),
  bids: makeSelectBids()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onSetCurrency: (currency) => dispatch(setCurrency(currency))
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'tradingPage', reducer });
const withSaga = injectSaga({ key: 'tradingPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(TradingPage);
