import { createSelector } from 'reselect';
import { fromJS } from 'immutable';

/**
 * Direct selector to the tradingPage state domain
 */
const selectMarketAnalysisProviderDomain = (state) => state.get('marketAnalysisProvider');
const selectTradingPageDomain = (state) => state.get('tradingPage');

/**
 * Other specific selectors
 */
export const makeSelectChartData = () => createSelector(
 [selectMarketAnalysisProviderDomain, selectTradingPageDomain],
 (providerState, tradingState) => {
   return providerState.getIn(['currencyData', tradingState.get('currency')])
 }
);

export const makeSelectTickerData = () => createSelector(
 [selectMarketAnalysisProviderDomain, selectTradingPageDomain],
 (providerState, tradingState) => providerState.getIn(['tickerData', tradingState.get('currency')])
);

export const makeSelectTradeHistory = () => createSelector(
 [selectMarketAnalysisProviderDomain, selectTradingPageDomain],
 (providerState, tradingState) => {
   const trades = providerState.getIn(['trades', tradingState.get('currency')]);
   return trades || fromJS([]);
 }
);

export const makeSelectAsks = () => createSelector(
 [selectMarketAnalysisProviderDomain, selectTradingPageDomain],
 (providerState, tradingState) => providerState.getIn(['asks', tradingState.get('currency')])
);

export const makeSelectBids = () => createSelector(
 [selectMarketAnalysisProviderDomain, selectTradingPageDomain],
 (providerState, tradingState) => providerState.getIn(['bids', tradingState.get('currency')])
);


/**
 * Default selector used by TradingPage
 */

const makeSelectTradingPage = () => createSelector(
  selectTradingPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectTradingPage;
export {
  selectTradingPageDomain,
};
